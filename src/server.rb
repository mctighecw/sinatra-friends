require 'sinatra'
require 'sinatra/namespace'
require 'sinatra/reloader' if development?
require 'json'

require './db/db'
require './db/models'
require './db/helpers'

environment = ENV['APP_ENV'] || 'development'

get '/' do
  redirect '/api/test/hello'
end

# test routes
namespace '/api/test' do
  before do
    content_type 'application/json'
  end

  get '/mode' do
    "Server has started in #{environment} mode"
  end

  get '/hello' do
    'Hello World!'
  end

  get '/hello/:name' do |name|
    "Hello #{name}!"
  end
end

# friends routes
namespace '/api/friends' do
  before do
    content_type 'application/json'
  end

  get '/all' do
    friends = Friend.all
    friends.map { |friend| FriendSerializer.new(friend) }.to_json
  end

  get '/find' do
    friends = Friend.all

    [:name, :age, :friend_group].each do |filter|
      friends = friends.send(filter, params[filter]) if params[filter]
    end

    friends.map { |friend| FriendSerializer.new(friend) }.to_json
  end

  post '/new' do
    params = JSON.parse request.body.read

    begin
      puts params['name']
      new_item = Friend.new(
        name: params['name'],
        age: params['age'],
        friend_group: params['friend_group']
      )
      new_item.save
      { 'status': 'OK' }.to_json

    rescue
      { 'status': 'Error' }.to_json

    end
  end

  post '/delete' do
    params = JSON.parse request.body.read

    begin
      puts params['id']
      Friend.where(_id: params['id']).delete
      { 'status': 'OK' }.to_json

    rescue
      { 'status': 'Error' }.to_json

    end
  end
end
