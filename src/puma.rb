workers      2
threads      1, 5
rackup       DefaultRackup
port         4567
environment  ENV['APP_ENV'] || 'development'

preload_app!
