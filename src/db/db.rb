require 'mongoid'

environment = ENV['APP_ENV'] || 'development'

Mongoid.load!('./db/mongoid.config', environment)
