require 'mongoid'

class Friend
  include Mongoid::Document
  store_in collection: "friends", database: "friends"

  field :name, type: String
  field :age, type: Integer
  field :friend_group, type: String

  validates :name, presence: true
  validates :age, presence: true
  validates :friend_group, presence: true

  scope :name, -> (name) {
    where(name: /^#{name}/)
  }
  scope :age, -> (age) {
    where(age: age)
  }
  scope :friend_group, -> (friend_group) {
    where(friend_group: friend_group)
  }
end
