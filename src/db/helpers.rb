class FriendSerializer
  def initialize(friend)
    @friend = friend
  end

  def as_json(*)
    data = {
      id: @friend.id.to_s,
      name: @friend.name,
      age: @friend.age,
      friend_group: @friend.friend_group
    }

    data[:errors] = @friend.errors if @friend.errors.any?
    data
  end
end
