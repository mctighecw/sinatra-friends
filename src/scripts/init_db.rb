require './db/db'
require './db/models'

seeds_data = [
  {
    :name => 'John',
    :age => 25,
    :friend_group => 'childhood'
  },
  {
    :name => 'Susan',
    :age => 22,
    :friend_group => 'childhood'
  },
  {
    :name => 'Sam',
    :age => 27,
    :friend_group => 'college'
  },
  {
    :name => 'Robert',
    :age => 26,
    :friend_group => 'work'
  },
  {
    :name => 'Anna',
    :age => 24,
    :friend_group => 'work'
  },
  {
    :name => 'Greg',
    :age => 26,
    :friend_group => 'meetup'
  }
]

puts "Initializing database..."

seeds_data.each do |values|
  puts values[:name]

  new_item = Friend.new(
    name: values[:name],
    age: values[:age],
    friend_group: values[:friend_group]
  )
  new_item.save
end
