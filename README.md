# README

My first project using the Ruby micro-framework `Sinatra`, along with MongoDB.

Sinatra is lightweight, minimalistic, and easy to use. It was the project that inspired Flask, a well-known Python micro-framework. It is much leaner and simpler than Ruby on Rails.

## App Information

App Name: sinatra-friends

Created: October 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/sinatra-friends)

## Tech Stack

- Ruby 2.6
- [Sinatra](http://sinatrarb.com/)
- [Mongoid ODM](https://docs.mongodb.com/mongoid/current/)
- [MongoDB](https://www.mongodb.com)
- [rbenv](https://github.com/rbenv/rbenv)

## To Run

1. Setup

```
$ cd src
$ bundle install
```

2. Initialize database

```
$ cd src
$ ruby ./scripts/init_db.rb
```

3. Drop database (if necessary)

```
$ cd src
$ ruby ./scripts/drop_db.rb
```

4. Connect to MongoDB via terminal (if necessary)

```
$ mongo -u admin --authenticationDatabase admin -p
```

5. Start development web server

```
$ cd src
$ bundle exec ruby server.rb
```

Go to: `localhost:4567`

## With Docker

- Build and start

```
$ docker-compose up -d --build
```

- Set up database

```
$ docker exec -it sinatra-friends_mongodb_1 bash
$ mongo
$ use admin
  (create admin user with password)
$ docker exec -it sinatra-friends_server_1 ruby ./scripts/init_db.rb
```

## Example Routes

- Get all items:

```
GET
URL: http://localhost:4567/api/friends/all
```

- Filter one or more:

```
GET
URL: http://localhost:4567/api/friends/find?name=S
```

- Add new item:

```
POST
DATA: { "name": "Ted", "age": 25, "friend_group": "college" }
URL: http://localhost:4567/api/friends/new
```

- Delete item:

```
POST
DATA: { "id": "5dad847d049b326454034a32" }
URL: http://localhost:4567/api/friends/delete
```

Last updated: 2025-02-27
